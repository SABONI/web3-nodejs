const mysql = require('mysql')
const fs = require('fs')

const connectionInfo = {
  'host': 'localhost',
  'user': 'root',
  'password': '',
  'database': 'ally'
}

function test(sqlString) {
  const db = mysql.createConnection(connectionInfo)
  db.connect((err) => {
    if (err) throw err
    console.log('Connected! alley DB')
    db.query(sqlString, (err, result) => {
      if (err) throw err
      console.log('Result: ' + JSON.stringify(result))
      db.end()
    })
  })
}

function select(sqlString, callback) {
  const db = mysql.createConnection(connectionInfo)
  let temp = 2
  db.connect((err) => {
    if (err) throw err
    console.log('Connected! alley DB')
    db.query(sqlString, (err, result, fields) => {
      if (err) throw err
      callback(result)
      // console.log(result);
      // temp = 3;//result[0]["key"]
      db.end()
    })
    temp = 3
  })
  return temp
}

exports.select = select

test('SELECT * FROM `ethalley`')
