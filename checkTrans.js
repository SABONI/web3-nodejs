const request = require('request')
const moment = require('moment')
const Web3 = require('web3')
const db2 = require('./DB_connect_eth_record.js')


const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/5c4afdac1639474d80d6569146657218'))
let timeStampList = []
let info
const mainAccount = '0xB86e9190fA864342dd0AA2E139CeC15B5C823D14'.toLowerCase()
// info => data from block
// timeStamplist => data from DB key_mongolia.history

function checkTrans() {
  function writeBalance(i) {
    // console.log(i)
    if (i < info.result.length) {
      let flag = true
      timeStampList.forEach((element) => {
        if (element.timestampBLK == info.result[i].timeStamp && element.account == info.result[i].from) {
          flag = false // already in history
        }
      })
      console.log(flag + ',' + info.result[i].timeStamp + ',' + info.result[i].to)
      if (flag && info.result[i].timeStamp > 1510545890 && info.result[i].to == mainAccount) {
        const dateString = moment(parseInt(info.result[i].timeStamp, 10) * 1000).utcOffset('+0800').format('YYYY/MM/DD/ HH:mm:ss')
        const sqlString = 'INSERT INTO `history` ( `account`, `action`, `value`,`type`,`creatAt`,`timestampBLK`,`transactionID`) Select \'' + info.result[i].from + '\',\'deposit\',' + web3.utils.fromWei(info.result[i].value, 'ether') + ',\'ETH\',\'' + dateString + '\',\'' + info.result[i].timeStamp + '\',\'' + info.result[i].hash + '\' from dual where not exists (SELECT * FROM `history` WHERE `timestampBLK` = \'' + info.result[i].timeStamp + '\' AND `account` = \'' + info.result[i].from + '\')'
        db2.select(sqlString, (ret) => { })
        const sqlString3 = "SELECT `sum` FROM `key` WHERE lower(`address`) = '" + info.result[i].from + "' "
        db2.select(sqlString3, (ret) => {
          const lastSum = ret
          console.log(lastSum)
          const sqlString4 = 'UPDATE `key` SET `sum`= ' + (lastSum[0].sum + parseFloat(web3.utils.fromWei(info.result[i].value, 'ether'))) + " WHERE lower(`address`) = '" + info.result[i].from + "'"
          db2.select(sqlString4, (ret) => { })
        })
      }
      setTimeout(() => { writeBalance(i + 1) }, 100)
    }
  }
  const sqlString2 = "SELECT `timestampBLK`,`account`,`value` FROM `history` WHERE `type` = 'ETH' and `timestampBLK` <> '' and `action` = 'deposit'"
  // https://api-ropsten.etherscan.io/api?module=account&action=txlist&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a&startblock=0&endblock=99999999&page=1&offset=10&sort=asc&apikey=YourApiKeyToken
  request('http://api-ropsten.etherscan.io/api?module=account&action=txlist&address=0xB86e9190fA864342dd0AA2E139CeC15B5C823D14&startblock=0&endblock=99999999&sort=asc&apikey=II7MEMV21VJX7PQDZR42JUDESNMIUCDYP3',
    (error, response, body) => {
      if (!error && response.statusCode == 200) {
        info = JSON.parse(body)
        db2.select(sqlString2, (ret) => {
          timeStampList = ret
          writeBalance(0)
        })
      }
    })
}

checkTrans()
setInterval(checkTrans, 120000)
