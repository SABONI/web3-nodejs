const Web3 = require('web3')
const db = require('./DB_connect_alley.js')
const db2 = require('./DB_connect_eth_record.js')

// var web3 = new Web3(new Web3.providers.HttpProvider("http://pub-node1.etherscan.io:8545/")); //web3 provider
const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/5c4afdac1639474d80d6569146657218'))
// execute = require('./DB_connect_alley.js')
const mainAccount = '0xB86e9190fA864342dd0AA2E139CeC15B5C823D14' // your main wallet

let privateKeys = [] // load private keys from DB
function polling() {
  db.select('SELECT `key` FROM `ethAlley`', (ret) => {
    privateKeys = ret
    privateKeys.forEach((ele) => {
      web3.eth.accounts.wallet.add('0x' + ele.key)
    })
    // console.log(web3.eth.accounts.wallet.length)
    function writeBalance(i) {
      if (i < web3.eth.accounts.wallet.length) {
        let balance
        web3.eth.getBalance(web3.eth.accounts.wallet[i].address).then((data) => {
          balance = data
          const tmp = parseInt(balance, 10) - 1300000000000000
          // console.log("i:"+i)
          if (tmp > 0) {
            console.log(tmp)
            console.log('i:' + i)
            web3.eth.sendTransaction({
              'from': web3.eth.accounts.wallet[i].address,
              'to': mainAccount,
              'value': tmp.toString(),
              'gas': 24000,
              'gasPrice': 20000000000
            }).on('transactionHash', (hash) => {
              console.log('transaction send :' + hash)
            }).on('receipt', (receipt) => {
              console.log('transaction confirm :' + receipt)
              // sqlString = 'INSERT INTO `history` ( `account`, `action`, `value`,`type`) Values (\'' +  web3.eth.accounts.wallet[i].address   +'\',\'' + 'deposit' + '\',' + web3.utils.fromWei(tmp,'ether') + ',\'' + "ETH" + '\')'; // 填進駐解裡面然後不要中文
            // db2.execute(sqlString, function(ret){});
            }).on('error', console.error)
          // db3.execute(sqlString, function(ret){});
          }
        }).then(writeBalance(i + 1)) // the getBalance finished then start writeBalance(i+1) & the(i) continue to end
      }
    } // function's end
    writeBalance(0)
  })
}
polling()
setInterval(polling, 60000)
