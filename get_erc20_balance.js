const Web3 = require('web3')


function erc20GetBalance(tokenAddress, walletAddress) {
  let web3 = new Web3()
  if (typeof web3 !== 'undefined') {
    web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/5c4afdac1639474d80d6569146657218')) // infura node
  } else { // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'))
  }
  // const tokenAddress = '0xd8062c267dd196b5e500a99ead7f50592f323e51' // bdb
  // const walletAddress = '0xB8235ed21f1352bBcac0f3AeCDB16f837A376760' // imtoken
  // The minimum ABI to get ERC20 Token balance
  const minABI = [
    // balanceOf
    {
      'constant': true,
      'inputs': [{ 'name': '_owner', 'type': 'address' }],
      'name': 'balanceOf',
      'outputs': [{ 'name': 'balance', 'type': 'uint256' }],
      'type': 'function'
    },
    // decimals
    {
      'constant': true,
      'inputs': [],
      'name': 'decimals',
      'outputs': [{ 'name': '', 'type': 'uint8' }],
      'type': 'function'
    }
  ]

  // Get ERC20 Token contract instance 這裡要用阻塞
  const contract = web3.eth.contract(minABI).at(tokenAddress)
  const balance = contract.balanceOf(walletAddress)
  const accountBalance = balance.div(10 ** contract.decimals().toNumber())
  console.log('Account: ' + walletAddress + 'Token: ' + tokenAddress + 'Balance: ' + accountBalance.toString())
  const accountbalance = accountBalance.toString()
  return accountbalance
}

exports.erc20GetBalance = erc20GetBalance
