const Web3 = require('web3')
const db2 = require('./DB_connect_eth_record.js')

const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/5c4afdac1639474d80d6569146657218'))
const withdrawPrivateKey = '' // withdraw wallet's private Key
let withdrawList = []
const sqlString = "SELECT * FROM `history` WHERE `type` = 'ETH' and `done` = '0' and `action` = 'withdraw'"
web3.eth.accounts.wallet.add(withdrawPrivateKey)

function withdraw() {
  db2.select(sqlString, (ret) => {
    withdrawList = ret
    web3.eth.accounts.wallet.add(withdrawPrivateKey)

    function takeMoney(i) {
      if (i < withdrawList.length) {
        // if(withdrawList[i].type=='ETH'&&withdrawList[i].done==0){
        const sqlString2 = "SELECT `sum` FROM `key` WHERE `address` = '" + withdrawList[i].account + "' "
        db2.select(sqlString2, (ret) => {
          const lastSum = ret
          if (lastSum[0].sum >= 0) {
            const tmp = parseFloat(((withdrawList[i].value - 0.001) * 0.9975).toFixed(8))
            web3.eth.sendTransaction({
              'from': web3.eth.accounts.wallet[0].address,
              'to': withdrawList[i].accountTo,
              'value': web3.utils.toWei(tmp.toString(), 'ether'),
              'gas': 24000,
              'gasPrice': 30000000000
            }).on('transactionHash', (hash) => {
              console.log(hash)
              const sqlString3 = "UPDATE `history` SET `done`='1' ,`transactionID`='" + hash + "' WHERE lower(accountTo) = '" + withdrawList[i].accountTo + "'"
              db2.execute(sqlString3, (ret) => {})
            }).on('receipt', (receipt) => {
            }).on('error', console.error)
          }
        })
      }
      // }
    }
    takeMoney(0)
  })
}
withdraw()
