const Web3 = require('web3')

let web3 = new Web3()
if (typeof web3 !== 'undefined') {
  web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/5c4afdac1639474d80d6569146657218'))
  //   web3 = new Web3(web3.currentProvider)
} else {
  web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/5c4afdac1639474d80d6569146657218'))
}

const tran = '0x8af44178859becbed4cdc5994bcd1f1ce53d1accce853461858fea907eb16872'

try {
  const tranR = web3.eth.getTransactionReceipt(tran)
  console.log(tranR)
} catch (err) {
  console.log(`ERROR :${err}`)
}

const account1 = '0x16c976872ceed4d3b36f7096ebd47ffd0721bffd'
web3.eth.getBalance(account1, (error, result) => {
  console.log(result)
})
