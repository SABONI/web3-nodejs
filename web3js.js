const Web3 = require('web3')

let web3 = new Web3()
if (typeof web3 !== 'undefined') {
  //  web3 = new Web3(new Web3.providers.HttpProvider('https://rinkeby.infura.io/538ED56FD649381FE05DCD856EB0E4CA2D429856ED6A971E5F2E515DB9949F55')) // infura node
  //  web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545')) // localhost
  //  web3 = new Web3(new Web3.providers.HttpProvider('https://rinkeby.infura.io/v3/5c4afdac1639474d80d6569146657218')) // infura node
  web3 = new Web3(web3.currentProvider)
} else {
  // set the provider you want from Web3.providers
  web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'))
}

web3.eth.getBalance(web3.eth.accounts[0], (error, result) => {
  console.log(result)
})

const accounts = web3.personal.listAccounts
console.log(accounts)
web3.eth.getBalance(web3.eth.accounts[0], (error, result) => {
  if (error) {
    console.error(error)
  } else {
    document.getElementById('0x2D190249e9782d661cd480448858470ddF13aa89').innerText = web3.fromWei(result.toNumber())
  }
})
