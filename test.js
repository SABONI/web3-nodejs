const Web3 = require('web3')
const Tx = require('ethereumjs-tx')

const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/5c4afdac1639474d80d6569146657218'))
const mainAccount = '0xB86e9190fA864342dd0AA2E139CeC15B5C823D14'
const Account1 = '0x16c976872ceed4d3b36f7096ebd47ffd0721bffd'
const Account2 = '0x6bE6c0d8f067566CB331d6f93d5deFd0166d1060'
const key = '538ED56FD649381FE05DCD856EB0E4CA2D429856ED6A971E5F2E515DB9949F55'
console.log(web3.eth.getBalance(mainAccount).toString())
let tmp = parseInt('1000000000000000000', 10) - 1300000000000000
console.log(tmp)

web3.eth.accounts.wallet.add('0x' + key)
console.log(web3.eth.accounts.wallet)
console.log(web3.eth.accounts.wallet[0].address)
web3.eth.sendTransaction({
  'from': web3.eth.accounts.wallet[0].address,
  'to': mainAccount,
  'value': tmp.toString(),
  'gas': 24000,
  'gasPrice': 20000000000
}).on('transactionHash', (hash) => {
  console.log('transaction send :' + hash)
}).on('receipt', (receipt) => {
  console.log('transaction confirm :' + receipt)
}).on('confirmation', (confirmationNumber, receipt) => {
  console.log(confirmationNumber)
}).on('error', console.error)
