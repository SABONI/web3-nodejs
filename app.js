const express = require('express')
const bodyParser = require('body-parser')
const getb = require('./get_erc20_balance.js')


const app = express()
app.use(bodyParser.urlencoded({ 'extended': false }))
// define routes here..
app.get('/', (req, res) => { res.sendFile(__dirname + 'templates/index.html') })

app.post('/submit-data', (req, res) => {
  const name = req.body.firstName + ' ' + req.body.lastName
  const tmp = ' Submitted Successfully!'
  const msg = name + tmp
  res.send(msg)
})

app.put('/update-data', (req, res) => { res.send('PUT Request') })

app.delete('/delete-data', (req, res) => { res.send('DELETE Request') })

app.get('/query', (req, res) => { res.sendFile(__dirname + 'templates/query.html') })

app.post('/query', (req, res) => {
  const msg = getb.erc20GetBalance(req.body.tokenAddress, req.body.walletAddress)
  res.send('Your token balace is ' + msg)
})


const server = app.listen(5050, () => { console.log('Node server is running..') })
